import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Image, TouchableOpacity, TouchableHighlight } from 'react-native';
import Opcaomenu from './Opcaomenu';
import { Actions } from 'react-native-router-flux';

export default class Principal extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <ImageBackground
        source={require('../imgs/menu3.jpg')} style={fundoprincipal.posicaoimg}>

        <View style={fundoprincipal.posicaotop}>
          <View style={fundoprincipal.logo}>
            <Image source={require('../imgs/SF.png')} />
          </View>
          <View style={fundoprincipal.menuop}>

            <TouchableOpacity style={fundoprincipal.opposicao} onPress={() => { Actions.options({ list: 'fruits' }); }}>
              <Opcaomenu opcaoText={'Frutas'} opcaoimg={require('../imgs/op2.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={fundoprincipal.opposicao} onPress={() => { Actions.options({ list: 'vegetables' }); }}>
              <Opcaomenu opcaoText={'Hortaliças'} opcaoimg={require('../imgs/op4.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={fundoprincipal.opposicao} onPress={() => { Actions.options({ list: 'legumes' }); }}>
              <Opcaomenu opcaoText={'Leguminosas e Enlatados'} opcaoimg={require('../imgs/op5.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={fundoprincipal.opposicao} onPress={() => { Actions.steps({ option: 'hands' }); }}>
              <Opcaomenu opcaoText={'Mãos'} opcaoimg={require('../imgs/op1.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={fundoprincipal.opposicao} onPress={() => { Actions.options({ list: 'utensils' }); }}>
              <Opcaomenu opcaoText={'Utensílios'} opcaoimg={require('../imgs/op3.png')} />
            </TouchableOpacity>




          </View>
        </View>
      </ImageBackground>
    );
  }
}
const fundoprincipal = StyleSheet.create({
  posicaoimg: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  posicaotop: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    flex: 8,
    justifyContent: 'center',
    paddingBottom: 30
  },
  menuop: {
    flex: 7,
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  opposicao: {
    width: '33.333333%',
    height: '50%',
    padding: 5,
  },
});