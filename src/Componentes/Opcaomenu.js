import React, { Component } from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';

export default class Opcaomenu extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={menu.opposicao}>
                <View style={menu.boxImg}>
                    <Image
                        source={this.props.opcaoimg}
                        style={menu.imagem} />
                </View>
                <View style={menu.text}>
                    <Text style={{ color: 'black', textAlign: 'center', fontSize: 12 }}>{this.props.opcaoText}</Text>
                </View>
            </View>
        );
    }
}
const menu = StyleSheet.create({
    imagem: {
        width: '100%',
        height: '98%',
        // backgroundColor:'red',
        padding: 50,
        // borderWidth: 3,

    },
    opposicao: {
        width: '100%',
        height: '100%',
        // backgroundColor:'red',
        padding: 0,
        marginBottom: 0,
        flex: 1,
    },
    boxImg: {
        height: '60%',
        width: '100%',
        padding: 50,
        paddingBottom: 25,
        paddingTop: 25,
        marginBottom: 8,
        // backgroundColor:'green',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        // flex:1,
        height: 35,
        width: '80%',
        alignSelf: 'center',
        backgroundColor: '#c5c5c5',
        // opacity:0.9,
        // borderWidth:1,
        borderRadius: 10,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }
});