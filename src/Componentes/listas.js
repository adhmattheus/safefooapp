const list = {
  listHandSteps: {
    img: require('../imgs/op1.png'),
    title: 'Higiene das mãos',
    steps: [
      {
        subTitle: 'Passo 1',
        teste: '',
        photo: require('../imgs/A1.jpg'),
        text: 'Abra a torneira e molhe as mãos.'
      },
      {
        subTitle: 'Passo 2',
        teste: 'Higiene das mãos',
        photo: require('../imgs/A2.jpg'),
        text: 'Aplique o sabão nas mãos.'
      },
      {
        subTitle: 'Passo 3',
        teste: 'Higiene das mãos',
        photo: require('../imgs/A3.jpg'),
        text: 'Ensaboe ambas as palmas e esfregue-as.'
      },
      {
        subTitle: 'Passo 4',
        teste: 'Higiene das mãos',
        photo: require('../imgs/A4.jpg'),
        text: 'Esfregue a parte de fora das mãos e entre os dedos.'
      },
      {
        subTitle: 'Passo 5',
        teste: 'Higiene das mãos',
        photo: require('../imgs/A5.jpg'),
        text: 'Esfregue a parte de fora das mãos e entre os dedos.'
      },
      {
        subTitle: 'Passo 6',
        teste: 'Higiene das mãos',
        photo: require('../imgs/A6.jpg'),
        text: 'Esfregue os polegares com a palma da mão.'
      },
      {
        subTitle: 'Passo 7',
        teste: 'Higiene das mãos',
        photo: require('../imgs/A7.jpg'),
        text: 'Limpe as unnhas utilizando uma escova .'
      },
      {
        subTitle: 'Passo 8',
        teste: 'Higiene das mãos',
        photo: require('../imgs/A8.jpg'),
        text: 'Lave os punhos com movimentos circulares.'
      },
      {
        subTitle: 'Passo 9',
        teste: 'Higiene das mãos',
        photo: require('../imgs/A9.jpg'),
        text: 'Retire o sabão.'
      },
      {
        subTitle: 'Passo 10',
        teste: 'Higiene das mãos',
        photo: require('../imgs/maos1.jpeg'),
        text: 'Seque as mãos.'
      },
    ]
  },
  listFruits: [

    {
      key: '1',
      items: ['Abacate'],
      img: require('../imgs/abacate.png')
    },
    {
      key: '2',
      items: ['Abacaxi'],
      img: require('../imgs/abacaxi.png')
    },

    {
      key: '9',
      items: ['Abóbora'],
      img: require('../imgs/abobora.png')

    },
    {
      key: '3',
      items: ['Goiaba'],
      img: require('../imgs/goiaba.png')
    },

    {
      key: '13',
      items: ['Kiwi'],
      img: require('../imgs/kiwi.png')
    },
    {
      key: '4',
      items: ['Laranja'],
      img: require('../imgs/laranja.png')

    },

    {
      key: '6',
      items: ['Maçã'],
      img: require('../imgs/maca.png')
    },
    {
      key: '11',
      items: ['Melancia'],
      img: require('../imgs/melancia.png')

    },
    {
      key: '12',
      items: ['Melão'],
      img: require('../imgs/melao.png')

    },
    {
      key: '10',
      items: ['Mamão'],
      img: require('../imgs/mamao.png')

    },
    {
      key: '7',
      items: ['Manga'],
      img: require('../imgs/manga.png')
    },
    {
      key: '8',
      items: ['Maracujá'],
      img: require('../imgs/maracuja.png')
    },
    {
      key: '14',
      items: ['Morango'],
      img: require('../imgs/morango.png')
    },
    {
      key: '5',
      items: ['Tangerina'],
      img: require('../imgs/tangerina.png')

    },
    {
      key: '15',
      items: ['Uva'],
      img: require('../imgs/uva.png')
    },
  ],
  listFruitsSteps: [
    {
      img: require('../imgs/abacateicon.png'),
      title: 'Abacate',
      steps: [
        {
          step: 'Passo 1',
          subTitle: 'Lavagem',
          photo: require('../imgs/abacatelavar.jpg'),
          text: 'Lave com água corrente.'
        },
        {
          step: 'Passo 2',
          subTitle: 'Desinfecção',
          photo: require('../imgs/B3.1.jpg'),
          text: 'Deixe de molho na solução de água clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
        },
        {
          step: 'Passo 3',
          subTitle: 'Enxágue',
          photo: require('../imgs/B4.1.jpg'),
          text: 'Lave com água para retirar o produto.'
        },
        {
          step: 'Passo 4',
          subTitle: 'Secagem',
          photo: require('../imgs/secagemaba.jpeg'),
          text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos .'
        },
        {
          step: 'Passo 5',
          subTitle: 'Armazenamento',
          photo: require('../imgs/B6.1.jpg'),
          text: 'Guarde em uma vasilha com tampa e refrigere.'
        },
        {
          step: 'Passo 6',
          subTitle: 'Exemplos',
          photo: require('../imgs/caju.jpg'),
          text: 'O mesmo processo é válido para o cajú.'
        }
      ]
    },
    {
      img: require('../imgs/abacaxiicon.jpg'),
      title: 'Abacaxi',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B8.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B9.jpg'),
            text: 'Deixe de molho na solução de água clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B10.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/B11.jpg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B12.jpeg'),
            text: 'Guarde no refrigerador.'
          }
        ]
    },
    {
      img: require('../imgs/goiabaicon.png'),
      title: 'Goiaba',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B2.jpg'),
            text: 'Lave com água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B3.jpg'),
            text: 'Deixe de molho na solução de água clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B4.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/goiabasecar.jpeg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B6.jpg'),
            text: 'Guarde em uma vasilha com tampa e refrigere.'
          },
          {
            step: 'Passo 6',
            subTitle: 'Exemplos',
            photo: require('../imgs/acerola.jpg'),
            text: 'O mesmo processo é válido para a acerola.'
          }
        ]
    },


    {
      img: require('../imgs/laranjaicon.jpg'),
      title: 'Laranja',
      steps:
        [

          {
            step: 'Passo 2',
            subTitle: 'Lavagem',
            photo: require('../imgs/B14.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B15.jpg'),
            text: 'Deixe de molho na solução de água clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Enxágue',
            photo: require('../imgs/B16.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Secagem.',
            photo: require('../imgs/laransecar.jpg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos .'
          },
          {
            step: 'Passo 6',
            subTitle: 'Armazenamento',
            photo: require('../imgs/laranrefri.jpeg'),
            text: 'Guarde no refrigerador.'
          }
        ]
    },
    {
      img: require('../imgs/tangerinaicon.jpg'),
      title: 'Tangerina',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem.',
            photo: require('../imgs/B16.1.jpg'),
            text: 'Lave com água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B15.1.jpg'),
            text: 'Deixe na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B16.1.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secage',
            photo: require('../imgs/tangesecar.jpg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos .'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/tangerefri.jpg'),
            text: 'Guarde no refrigerador.'
          },
          {
            step: 'Passo 6',
            subTitle: 'Exemplos',
            photo: require('../imgs/limao.jpg'),
            text: 'O mesmo processo é válido para o limão.'
          }
        ]
    },
    {
      img: require('../imgs/macaicon.png'),
      title: 'Maçã',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B20.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B21.jpg'),
            text: 'Deixe na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B22.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/B23.jpeg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B24.jpg'),
            text: 'Guarde em uma vasilha com tampa e refrigere.'
          }
        ]
    },
    {
      img: require('../imgs/mangaicon.jpg'),
      title: 'Manga',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B20.2.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B21.1.jpg'),
            text: 'Deixe de molho na solução de água clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B22.1.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/mangasecar.jpg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos .'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B24.1.jpg'),
            text: 'Guarde em vasilha com tampa e refrigere.'
          }
        ]
    },
    {
      img: require('../imgs/maracujaicon.jpg'),
      title: 'Maracujá',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B20.3.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B21.3.jpg'),
            text: 'Deixe na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B22.3.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/B23.3.jpeg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B24.3.jpg'),
            text: 'Guarde em uma vasilha com tampa e refrigere.'
          }
        ]
    },
    {
      img: require('../imgs/aboboraicon.png'),
      title: 'Abóbora',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B26.3.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B27.3.jpg'),
            text: 'Deixe na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B28.3.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/B29.jpeg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B30.4.jpg'),
            text: 'Guarde sob refrigeração. Após parti-la utilize o filme (pélicula de PVC) e armaneze.'
          }
        ]
    },
    {
      img: require('../imgs/mamaoicon.png'),
      title: 'Mamão',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B26.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B27.jpg'),
            text: 'Deixe na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B28.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/mamaosecar.jpg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B30.jpg'),
            text: 'Guarde sob refrigeração. Após parti-la utilize o filme (película de PVC) e armazene.'
          }
        ]
    },
    {
      img: require('../imgs/melanciaicon.png'),
      title: 'Melancia',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B26.4.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B27.4.jpg'),
            text: 'Deixe na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B28.4.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/B29.3.jpg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B30.3.jpeg'),
            text: 'Guarde sob refrigeração. Após parti-la utilize o filme (película de PVC) e armazene.'
          }
        ]
    },
    {
      img: require('../imgs/melaoicon.jpg'),
      title: 'Melão',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B26.1.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B27.1.jpg'),
            text: 'Deixe na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B28.2.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/B29.1.jpg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B30.1.jpg'),
            text: 'Guarde sob refrigeração. Após parti-la utilize o filme (película de PVC) e armazene.'
          }
        ]
    },
    {
      img: require('../imgs/kiwiicon.png'),
      title: 'Kiwi',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B32.1.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B33.1.jpg'),
            text: 'Deixe na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B34.1.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/B35.jpeg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B36.1.jpg'),
            text: 'Guarde em uma vasilha com tampa e refrigere.'
          }
        ]
    },
    {
      img: require('../imgs/morangoicon.png'),
      title: 'Morango',
      subTitle: 'Higiene das mãos',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B32.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B33.jpg'),
            text: 'Deixe na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/B34.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/B35.1.jpeg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B36.jpg'),
            text: 'Guarde em uma vasilha com tampa e refrigere.'
          }
        ]
    },
    {
      img: require('../imgs/op2.png'),
      title: 'Uva',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/B40.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Corte',
            photo: require('../imgs/B38.1.jpg'),
            text: 'Com auxílio de uma tesoura, corte as uvas sem retirar o talo.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Desinfecção',
            photo: require('../imgs/B39.jpg'),
            text: 'Deixe de molho na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Enxágue',
            photo: require('../imgs/B40.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Secagem',
            photo: require('../imgs/B41.1.jpeg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 6',
            subTitle: 'Armazenamento',
            photo: require('../imgs/B42.jpg'),
            text: 'Guarde em uma vasilha com tampa e refrigere.'
          }
        ]
    },



  ],
  listUtensils: [
    {
      key: '1',
      items: ['Copo'],
      img: require('../imgs/copo.png')
    },
    {
      key: '5',
      items: ['Esponja'],
      img: require('../imgs/esponja.png')

    },
    {
      key: '2',
      items: ['Prato'],
      img: require('../imgs/prato.png')
    },
    {
      key: '4',
      items: ['Talher'],
      img: require('../imgs/talher.png')
    },
    {
      key: '3',
      items: ['Vasilha',],
      img: require('../imgs/vasilha.png')
    },


  ],
  listUtensilsSteps: [
    {
      img: require('../imgs/copoicon.jpg'),
      title: 'Copo',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/C2.jpg'),
            text: 'Lave com água e sabão.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/C3.jpg'),
            text: 'Deixe de molho na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/C4.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/C5.1.jpeg'),
            text: 'Deixe secar naturalmente.'
          }
        ]
    },
    {
      img: require('../imgs/pratoicon.png'),
      title: 'Prato',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/C2.1.jpg'),
            text: 'Lave com água e sabão.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/C3.jpg'),
            text: 'Deixe de molho na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/C4.1.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/C5.2.jpeg'),
            text: 'Deixe secar naturalmente.'
          }
        ]
    },
    {
      img: require('../imgs/vasilhaicon.jpg'),
      title: 'Vasilha',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/C2.2.jpg'),
            text: 'Lave com água e sabão.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/C3.1.jpg'),
            text: 'Deixe de molho na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/C4.2.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/C5.3.jpeg'),
            text: 'Deixe secar naturalmente.'
          }
        ]
    },
    {
      img: require('../imgs/op3.png'),
      title: 'Talher',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/C7.jpg'),
            text: 'Lave com água e sabão.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/C8.jpg'),
            text: 'Borrife o álcool, concentração de 70% na superfície, já que cloro corrói os metais.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Secagem',
            photo: require('../imgs/C10.jpg'),
            text: 'Deixe secar naturalmente.'
          }
        ]
    },
    {
      img: require('../imgs/esponjaicon.jpg'),
      title: 'Esponja',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/C12.jpg'),
            text: 'Lave com água e sabão.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/C13.jpg'),
            text: 'Deixe de molho na solução de água clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Desinfecção',
            photo: require('../imgs/C13.1.jpg'),
            text: 'ou manter sob fervura por 5 minutos.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Enxágue',
            photo: require('../imgs/C14.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Secagem',
            photo: require('../imgs/C15.jpeg'),
            text: 'Deixar secar naturalmente.'
          }
        ]
    },
  ],
  listLegumes: [
    {
      key: '1',
      items: ['Leguminosas'],
      img: require('../imgs/leguminosas.png')
    },
    {
      key: '2',
      items: ['Enlatados'],
      img: require('../imgs/enlatados.png')
    }
  ],
  listLegumesSteps: [
    {
      img: require('../imgs/op5.png'),
      title: 'Leguminosas',
      steps:
        [
          /*{
            step: 'Preparação',
            photo: require('../imgs/E1.jpg'),
          },*/
          {
            step: 'Passo 1',
            subTitle: 'Armazenamento',
            photo: require('../imgs/feijaoapp1.jpeg'),
            text: 'Antes de armazenar, higienize as embalagens borrifando álcool (70%).'
          },
          {
            step: 'Passo 1',
            subTitle: 'Armazenamento',
            photo: require('../imgs/feijaoapp2.jpeg'),
            text: 'ou solução clorada (1 colher de sopa de água sanitária para cada litro de água).'
          },
          {
            step: 'Passo 1',
            subTitle: 'Armazenamento',
            photo: require('../imgs/E2.jpg'),
            text: 'Após a abertura da embalagem original, transfira o conteúdo para um pote com tampa.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Identificação',
            photo: require('../imgs/E3.jpg'),
            text: 'E identifique com  etiqueta contendo, nome do produto, data de abertura e data de validade.'
          }
        ]
    },
    {
      img: require('../imgs/leguminosasicon.png'),
      title: 'Enlatados',
      steps:
        [
          {
            step: 'Preparação',
            subTitle: 'Preparo',
            photo: require('../imgs/lata4.jpeg'),
            text: 'Antes de armazenar, higienize as embalagens.'

          },
          {
            step: 'Higienização',
            subTitle: 'Higienização',
            photo: require('../imgs/lata3.jpeg'),
            text: 'Borrife álcool 70%...'

          },
          {
            step: 'Higienização',
            subTitle: 'Higienização',
            photo: require('../imgs/lata2.jpeg'),
            text: 'ou lave com água e sabão'
          },
          {
            step: 'Passo 1',
            subTitle: 'Armazenamento',
            photo: require('../imgs/lata1.jpeg'),
            text: 'Espere secar e armazene em local seco, arejado e sem insetos ou roedores.'
          },

          {
            step: 'Passo 1',
            subTitle: 'Armazenamento',
            photo: require('../imgs/E6.jpg'),
            text: 'Apos a abertura da embalagem original, transfira o conteúdo para um pote com tampa.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Identificação',
            photo: require('../imgs/E6.jpg'),
            text: 'Indentifique com etiqueta contendo nome, data de validade e data de abertura.'
          }
        ]
    },
  ],
  listVegetables: [
    {
      key: '1',
      items: ['Acelga'],
      img: require('../imgs/acelga.png')
    },
    {
      key: '2',
      items: ['Batata'],
      img: require('../imgs/batata.png')
    },
    {
      key: '4',
      items: ['Cenoura'],
      img: require('../imgs/cenoura.png')
    },
    {
      key: '5',
      items: ['Pimentão',],
      img: require('../imgs/pimentao.png')
    },
    {
      key: '3',
      items: ['Tomate'],
      img: require('../imgs/tomate.png')
    },


  ],
  listVegetablesSteps: [
    {
      img: require('../imgs/op4.png'),
      title: 'Acelga',
      subTitle: 'Higiene das mãos',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/D1.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/D2.jpg'),
            text: 'Deixe de molho na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/D3.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/D4.jpeg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/D5.jpg'),
            text: 'Utilize o papel toalha entre as folhas.'
          },
          {
            step: 'Passo 6',
            subTitle: 'Alface',
            photo: require('../imgs/alface.jpg'),
            text: 'O mesmo processo é válido para o alface.'
          },
        ]
    },
    {
      img: require('../imgs/batataicon.jpg'),
      title: 'Batata',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/D6.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Secagem',
            photo: require('../imgs/D9.jpeg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Armazenamento',
            photo: require('../imgs/D10.jpg'),
            text: 'Guarde em uma vasilha com tampa e refrigere.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Batata-Doce',
            photo: require('../imgs/batatadoce.jpg'),
            text: 'O mesmo processo é válido para a batata-doce.'
          },
        ]
    },
    {
      img: require('../imgs/tomateicon.jpg'),
      title: 'Tomate',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/D12.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/D11.jpg'),
            text: 'Deixe de molho na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/D12.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/D14.jpeg'),
            text: 'Deixar secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/D15.jpg'),
            text: 'Guarde em uma vasilha com tampa e refrigere.'
          },
        ]
    },
    {
      img: require('../imgs/cenouraicon.jpg'),
      title: 'Cenoura.',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem.',
            photo: require('../imgs/D16.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Secanagem.',
            photo: require('../imgs/cenouraatualizar.jpeg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Armazenamento.',
            photo: require('../imgs/D20.jpg'),
            text: 'Guarde em vasilha com tampa no refrigerador.'
          }
        ]
    },
    {
      img: require('../imgs/pimentaoicon.jpg'),
      title: 'Pimentão',
      steps:
        [
          {
            step: 'Passo 1',
            subTitle: 'Lavagem',
            photo: require('../imgs/D21.jpg'),
            text: 'Lave em água corrente.'
          },
          {
            step: 'Passo 2',
            subTitle: 'Desinfecção',
            photo: require('../imgs/D22.jpg'),
            text: 'Deixe de molho na solução clorada por 30 minutos. Adicione 1 colher de sopa de água sanitária (2%) para cada litro de água.'
          },
          {
            step: 'Passo 3',
            subTitle: 'Enxágue',
            photo: require('../imgs/D23.jpg'),
            text: 'Lave com água para retirar o produto.'
          },
          {
            step: 'Passo 4',
            subTitle: 'Secagem',
            photo: require('../imgs/D24.jpeg'),
            text: 'Deixe secar naturalmente em ambiente livre de poeira e insetos.'
          },
          {
            step: 'Passo 5',
            subTitle: 'Armazenamento',
            photo: require('../imgs/D25.jpg'),
            text: 'Guarde em uma vasilha com tampa e refrigere.'
          },
        ]
    },
  ],
  listEggsSteps: {
    title: 'Ovos.',
    img: require('../imgs/op6.png'),
    subTitle: 'Higiene das mãos',
    steps: [
      {
        step: 'Passo 1',
        subTitle: 'Armazenamento',
        photo: require('../imgs/F2.jpg'),
        text: 'Devem ser retirados das embalagens e armazenados sob refrigeração em local separado de outros aliementos.'
      },
      {
        step: 'Passo 2',
        subTitle: 'Desinfecção',
        photo: require('../imgs/F3.jpeg'),
        text: 'Permanecer de molho na solução de água sanitária, uma colher de sopa para cada litro de água (2%), por 7,5 minutos.'
      },
      {
        step: 'Passo 3',
        subTitle: 'Lavagem',
        photo: require('../imgs/F4.jpg'),
        text: 'Somente devem ser lavados imediatamente antes de sua utilização para, não perder a proteção natural.'
      },
      {
        step: 'Passo 4',
        subTitle: 'Cocção',
        photo: require('../imgs/F5.jpeg'),
        text: 'Após isso estarão prontos para cocção.'
      },
    ]
  }
}
export default list;