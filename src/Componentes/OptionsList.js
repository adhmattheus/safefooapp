import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Text, Card} from 'native-base';
import {Actions} from 'react-native-router-flux';
import list from './listas';

export default class OptionsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      router: '',
    };
  }
  componentDidMount() {
    switch (this.props.list) {
      case 'fruits': {
        this.setState({list: list.listFruits});
        break;
      }
      case 'utensils': {
        this.setState({list: list.listUtensils});
        break;
      }
      case 'legumes': {
        this.setState({list: list.listLegumes});
        break;
      }
      case 'vegetables': {
        this.setState({list: list.listVegetables});
        break;
      }
    }
  }
  render() {
    return (
      <FlatList
        data={this.state.list}
        renderItem={({item}) => (
          <TouchableOpacity
            key={item.key}
            onPress={() => {
              switch (this.props.list) {
                case 'fruits': {
                  switch (item.key) {
                    case '1': {
                      Actions.steps({option: 'f1'});
                      break;
                    }
                    case '2': {
                      Actions.steps({option: 'f2'});
                      break;
                    }
                    case '3': {
                      Actions.steps({option: 'f3'});
                      break;
                    }
                    case '4': {
                      Actions.steps({option: 'f4'});
                      break;
                    }
                    case '5': {
                      Actions.steps({option: 'f5'});
                      break;
                    }
                    case '6': {
                      Actions.steps({option: 'f6'});
                      break;
                    }
                    case '7': {
                      Actions.steps({option: 'f7'});
                      break;
                    }
                    case '8': {
                      Actions.steps({option: 'f8'});
                      break;
                    }
                    case '9': {
                      Actions.steps({option: 'f9'});
                      break;
                    }
                    case '10': {
                      Actions.steps({option: 'f10'});
                      break;
                    }
                    case '11': {
                      Actions.steps({option: 'f11'});
                      break;
                    }
                    case '12': {
                      Actions.steps({option: 'f12'});
                      break;
                    }
                    case '13': {
                      Actions.steps({option: 'f13'});
                      break;
                    }
                    case '14': {
                      Actions.steps({option: 'f14'});
                      break;
                    }
                    case '15': {
                      Actions.steps({option: 'f15'});
                      break;
                    }
                  }
                  break;
                }
                case 'utensils': {
                  switch (item.key) {
                    case '1': {
                      Actions.steps({option: 'u1'});
                      break;
                    }
                    case '2': {
                      Actions.steps({option: 'u2'});
                      break;
                    }
                    case '3': {
                      Actions.steps({option: 'u3'});
                      break;
                    }
                    case '4': {
                      Actions.steps({option: 'u4'});
                      break;
                    }
                    case '5': {
                      Actions.steps({option: 'u5'});
                      break;
                    }
                  }
                  break;
                }
                case 'legumes': {
                  switch (item.key) {
                    case '1': {
                      Actions.steps({option: 'l1'});
                      break;
                    }
                    case '2': {
                      Actions.steps({option: 'l2'});
                      break;
                    }
                  }
                  break;
                }
                case 'vegetables': {
                  switch (item.key) {
                    case '1': {
                      Actions.steps({option: 'v1'});
                      break;
                    }
                    case '2': {
                      Actions.steps({option: 'v2'});
                      break;
                    }
                    case '3': {
                      Actions.steps({option: 'v3'});
                      break;
                    }
                    case '4': {
                      Actions.steps({option: 'v4'});
                      break;
                    }
                    case '5': {
                      Actions.steps({option: 'v5'});
                      break;
                    }
                  }
                  break;
                }
              }
            }}>
            <Card style={styles.cardStyle}>
              <ImageBackground style={styles.imageBackCard} source={item.img}>
                <View style={styles.viewCard}>
                  {item.items.map((el, index) => (
                    <Text style={styles.textStyle} key={index}>
                      {el}
                    </Text>
                  ))}
                </View>
              </ImageBackground>
            </Card>
          </TouchableOpacity>
        )}
      />
    );
  }
}
const styles = StyleSheet.create({
  img: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  textStyle: {
    fontSize: 26,
  },
  viewCard: {
    flex: 1,
    paddingLeft: 10,
  },
  imageBackCard: {
    height: '100%',
    width: '100%',
  },
  cardStyle: {
    width: '100%',
    height: 150,
    padding: 2,
  },
});
