import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
  Button,
} from 'react-native';

import {Card, CardItem, Thumbnail, Left, Body} from 'native-base';

import Icon from 'react-native-vector-icons/MaterialIcons';

import list from './listas';

import Carousel from 'react-native-snap-carousel';

export default class StepsView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      index: 0,
      _carousel: 'teste',
      title: '',
      subTitle: '',
      stepList: {
        title: '',
        steps: [],
        img: require('../imgs/op2.png'),
      },
    };
  }

  renderItem = ({item, index}) => {
    return (
      <View key={index} style={{flex: 9}}>
        <CardItem style={{backgroundColor: 'white', padding: 0, margin: 0}}>
          <Left style={{backgroundColor: 'white'}}>
            <Thumbnail source={this.state.stepList.img} />
            <Body>
              <Text style={{fontSize: 19, color: 'black'}}>
                {this.state.stepList.title}
              </Text>
              <Text style={{fontSize: 14, color: 'black'}}>
                {/* {el.subTitle} */}
                {this.state.stepList.steps[index].subTitle}
              </Text>
            </Body>
          </Left>
        </CardItem>
        <Card key={index} style={{flex: 1}}>
          <View style={{flex: 1}}>
            <View style={{flex: 4}}>
              <Image
                style={{flex: 1, width: '100%', height: '100%'}}
                // source={el.photo}
                source={this.state.stepList.steps[index].photo}
              />
            </View>
            <View style={{flex: 1, padding: 10}}>
              <Text style={{fontSize: 17, color: 'black'}}>
                {/* {el.text} */}
                {this.state.stepList.steps[index].text}
              </Text>
            </View>
          </View>
        </Card>
      </View>
    );
  };

  returnList = () => {
    switch (this.props.option) {
      case 'hands': {
        return list.listHandSteps;
      }
      case 'f1': {
        return list.listFruitsSteps[0];
      }
      case 'f2': {
        return list.listFruitsSteps[1];
      }
      case 'f3': {
        return list.listFruitsSteps[2];
      }
      case 'f4': {
        return list.listFruitsSteps[3];
      }
      case 'f5': {
        return list.listFruitsSteps[4];
      }
      case 'f6': {
        return list.listFruitsSteps[5];
      }
      case 'f7': {
        return list.listFruitsSteps[6];
      }
      case 'f8': {
        return list.listFruitsSteps[7];
      }
      case 'f9': {
        return list.listFruitsSteps[8];
      }
      case 'f10': {
        return list.listFruitsSteps[9];
      }
      case 'f11': {
        return list.listFruitsSteps[10];
      }
      case 'f12': {
        return list.listFruitsSteps[11];
      }
      case 'f13': {
        return list.listFruitsSteps[12];
      }
      case 'f14': {
        return list.listFruitsSteps[13];
      }
      case 'f15': {
        return list.listFruitsSteps[14];
      }
      case 'u1': {
        return list.listUtensilsSteps[0];
      }
      case 'u2': {
        return list.listUtensilsSteps[1];
      }
      case 'u3': {
        return list.listUtensilsSteps[2];
      }
      case 'u4': {
        return list.listUtensilsSteps[3];
      }
      case 'u5': {
        return list.listUtensilsSteps[4];
      }
      case 'l1': {
        return list.listLegumesSteps[0];
      }
      case 'l2': {
        return list.listLegumesSteps[1];
      }
      case 'v1': {
        return list.listVegetablesSteps[0];
      }
      case 'v2': {
        return list.listVegetablesSteps[1];
      }
      case 'v3': {
        return list.listVegetablesSteps[2];
      }
      case 'v4': {
        return list.listVegetablesSteps[3];
      }
      case 'v5': {
        return list.listVegetablesSteps[4];
      }
      case 'eggs': {
        return list.listEggsSteps;
      }
    }
  };

  selectSubTitle = index => {
    this.setState({
      index,
    });
    // this.setState({step:(index+1)})
  };

  componentDidMount() {
    this.setState({
      stepList: this.returnList(),
      auxList: this.returnList(),
    });
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, padding: 0, margin: 0}}>
          <Carousel
            ref={c => {
              this._carousel = c;
            }}
            scrollEnabled={true}
            data={this.state.stepList.steps}
            renderItem={this.renderItem}
            sliderWidth={Dimensions.get('window').width}
            itemWidth={Dimensions.get('window').width}
            sliderHeight={Dimensions.get('window').height}
          />
          {/* <View style={{backgroundColor: 'red', height: 100}} /> */}
          <TouchableOpacity
            style={{
              height: 60,
              width: 60,
              borderRadius: 1,
              position: 'absolute',
              top: '50%',
              left: -15,
            }}
            onPress={() => this._carousel.snapToPrev()}>
            <Icon name="navigate-before" size={70} color={'white'} />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: 60,
              width: 60,
              borderRadius: 1,
              position: 'absolute',
              top: '50%',
              left: '86%',
            }}
            onPress={() => this._carousel.snapToNext()}>
            <Icon name="navigate-next" size={70} color={'white'} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
