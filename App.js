import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Principal from './src/Componentes/Principal';
import StepsView from './src/Componentes/StepsView';
import OptionsList from './src/Componentes/OptionsList';


export default class App extends Component {
    render() {
        return (
            <Router>
                <Scene key="root">
                    <Scene key='principal' hideNavBar={true} component={Principal} initil />
                    <Scene key='steps' component={StepsView} />
                    <Scene key='options' component={OptionsList} />
                </Scene>
            </Router>
        );
    }
}